<?php
  require_once('config.php');

  if ($main_settings['main_site_disable']==1 and empty($_SESSION['AID'])) {
    $smarty->display('reconstraction.tpl');
    exit();
  }

  $_SESSION['ID'] = isset($_SESSION['ID']) ? $_SESSION['ID'] : 0;
  if (($_SESSION['ID']==0) and !empty($_COOKIE['ID']))
    $_SESSION['ID'] = $_COOKIE['ID'];

  if (($_SESSION['ID'] != 0) and ($sql->fetchOne("SELECT ban FROM users WHERE id={$_SESSION['ID']}", "ban"))){
      $_SESSION['ID'] = 0;
  	  $smarty->assign('show_ban_message', '1');
  }

  $show = isset($_GET['show']) ? $_GET['show'] : "main";
  $subshow = isset($_GET['subshow']) ? $_GET['subshow'] : "";
  $time_type = isset($_GET['time_type']) ? $_GET['time_type'] : "all";
  $site_data = array();

  if (isset($_SESSION['express_end_tmp']) and
    ($show!='404') and
    !(($show=='my') and isset($_GET['subshow']) and (($_GET['subshow']=='buy') or ($_GET['subshow']=='buyajax'))))
    restore_express_end();  // пользователь отказался от покупки экспресс-аукциона

  $plugins->loadExtentionFile("affiliate", "include/index_set_cookie.php");

  if(isset($_GET['setcat']))
    $setcat = ($_GET['setcat']=='all') ? '' : mysql_escape_string($_GET['setcat']);
  if (!empty($setcat)) {
    $catid = $sql->fetchOne("SELECT cid FROM categories WHERE url_alias='$setcat'", 'cid');
     $_SESSION['CATID'] = (!empty($catid)) ? $catid : (integer)$setcat;
  }
  else {
    if (isset($_SESSION['CATID']))
      unset($_SESSION['CATID']);
  }
  if (isset($_SESSION['CATID']))
    $site_data['category_title'] = sql_get_category_title($_SESSION['CATID']);

  $meta = Settings::getSettingsByKey('meta');
  $smarty->assign($meta);

  if ($show=='rss') {
      include_once("./controllers/RssController.php");
  	  exit;
  }
  elseif ($show == "invite")
  	include_once("./controllers/InviteController.php");
  elseif ($show == "activate")
      include_once("./controllers/ActivationController.php");
  elseif ($show == "logout")
      include_once("./controllers/LogoutController.php");
  elseif ($show == "login")
      include_once("./controllers/LoginController.php");
  elseif ($show == 'subscribe')
      include_once("./controllers/SubscribeController.php");
  // else if ($show == '404') {
      // header("HTTP/1.0 404 Not Found");
      // $site_data['content'] = $smarty->display('404.tpl');
      // exit;
  // }

  bonusbalance_check();  // проверка бонусных начислений

  if (getId()) {
      $user_id = $user_class->getId();
      $user_info = $user_class->getUserInfo($user_id);
      $smarty->assign('user_info', $user_info);

      if ((time() - $_SESSION['LLTIME']) < (ONLINE_CHECK * 60)) {
          $_SESSION['LLTIME'] = time();
          $sql->Req("update users set lastlogin=now() where id='" . $_SESSION['ID'] . "';");
      }

      $pays = $sql->fetchOne("SELECT COUNT(*) AS cnt FROM pay_outcomming WHERE userid={$_SESSION['ID']} AND status=1 AND payvariant != 'bonus'", "cnt");
      $bids_active = (($main_settings['register_bonus_type']==1) and ($pays==0)) ? 0 : 1;
      $smarty->assign('bids_active', $bids_active);
  }
  else
      $user_id = 0;
  $smarty->assign('user_id', $user_id);

  if ($show=="main") {
     $smarty->assign('mainpage', true);
     $site_data['main_banners'] = $sql->fetchAll("SELECT * FROM banners WHERE is_enable=1 AND lang='{$_SESSION['front_content_lang']}' ORDER BY position");
  }

  $site_data['show'] = $show;
  $site_data['subshow'] = $subshow;
  $site_data['mobile'] = check_smartphone();
  $site_data['design'] = $design;
  $site_data['metrika'] = Settings::getSettingsByKey('counters');
  $site_data['social']  = Settings::getSettingsByKey('social');
  $site_data['extension_head']  = $plugins->head();
  $site_data['extension_body'] = $plugins->UserContent;
  $smarty->assign('menu', getMenu());  // load menu with categories

  $site_data['partners_program_active'] = $sql->fetchOne("SELECT value FROM main_settings WHERE `key`='makebids_lvl_cnt'", "value");

  $site_data['top_menu'] = file_exists(BASE_PATH."/upload/menu/top_menu.tpl") ?
    file_get_contents(BASE_PATH."/upload/menu/top_menu.tpl") :
    file_get_contents(BASE_PATH."/inc/templates/$design/templates/top_menu_default.tpl");
  $site_data['bottom_menu'] = file_exists(BASE_PATH."/upload/menu/bottom_menu.tpl") ?
    file_get_contents(BASE_PATH."/upload/menu/bottom_menu.tpl") :
    file_get_contents(BASE_PATH."/inc/templates/$design/templates/bottom_menu_default.tpl");

  if (file_exists("inc/applications/$design/index.php"))  // если нужно собрать ещё данные для конкретного дизайна
    include("inc/applications/$design/index.php");

  $smarty->assign($site_data);

  $path = get_path("$show.php");
  if (!empty($path))
    $content = include($path);
  elseif ($show == "about")
    $content = contentPage('about.htm');
  elseif ($show == "terms")
  	$content = contentPage("terms.htm");

  $smarty->assign('content', $content);
  $smarty->display('index.tpl');
?>